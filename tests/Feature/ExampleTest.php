<?php

namespace Velcoda\Commands\ArtisanLib\Tests\Feature;

use Velcoda\Commands\ArtisanLib\Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
