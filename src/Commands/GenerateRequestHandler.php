<?php

namespace Velcoda\Commands\Commands;

use Illuminate\Console\GeneratorCommand;
/**
 * List all locally installed packages.
 *
 * @author JeroenG
 **/
class GenerateRequestHandler extends GeneratorCommand
{
    /**
     * The name of your command.
     * This is how your Artisan's command shall be invoked.
     */
    protected $signature = 'tf:request_handler {name} {--apiv=}';

    /**
     * A short description of the command's purpose.
     * You can see this working by executing
     * php artisan list
     */
    protected $description = 'Create a RequestHandler for the Transaction Flow';


    /**
     * Class type that is being created.
     * If command is executed successfully you'll receive a
     * message like this: $type created succesfully.
     * If the file you are trying to create already
     * exists, you'll receive a message
     * like this: $type already exists!
     */
    protected $type = 'RequestHandler';

    /**
     * Specify your Stub's location.
     */
    protected function getStub()
    {
        return  __DIR__ . DIRECTORY_SEPARATOR . '../stubs/RequestHandlerExample.stub';
    }

    /**
     * The root location where your new file should
     * be written to.
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        $version = $this->option('apiv');

        return $rootNamespace . '\Http\RequestHandlers' . '\\' . $version;
    }
}
