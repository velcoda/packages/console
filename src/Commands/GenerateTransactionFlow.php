<?php

namespace Velcoda\Commands\Commands;

use Illuminate\Console\Command;

/**
 * List all locally installed packages.
 *
 * @author JeroenG
 **/
class GenerateTransactionFlow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create UseCase and Request Handler for TransactionFlow';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $name = $this->ask('What is your the name of the TransactionFlow?');
        $version = $this->anticipate('What API Version?', ['V1'], 'V1');

        $this->call('tf:use_case', [ 'name' => $name . 'UC', '--apiv' => $version ]);
        $this->call('tf:request_handler', [ 'name' => $name . 'RH', '--apiv' => $version ]);
    }
}
