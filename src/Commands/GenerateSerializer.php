<?php

namespace Velcoda\Commands\Commands;

use Illuminate\Console\GeneratorCommand;
/**
 * List all locally installed packages.
 *
 * @author JeroenG
 **/
class GenerateSerializer extends GeneratorCommand
{
    /**
     * The name of your command.
     * This is how your Artisan's command shall be invoked.
     */
    protected $signature = 'vlc:serializer {name}';

    /**
     * A short description of the command's purpose.
     * You can see this working by executing
     * php artisan list
     */
    protected $description = 'Create a Serializer';


    /**
     * Class type that is being created.
     * If command is executed successfully you'll receive a
     * message like this: $type created succesfully.
     * If the file you are trying to create already
     * exists, you'll receive a message
     * like this: $type already exists!
     */
    protected $type = 'Serializer';

    /**
     * Specify your Stub's location.
     */
    protected function getStub()
    {
        return __DIR__ . DIRECTORY_SEPARATOR . '../stubs/SerializerExample.stub';
    }

    /**
     * The root location where your new file should
     * be written to.
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\Http\Serializers';
    }
}
