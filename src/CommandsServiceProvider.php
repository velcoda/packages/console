<?php

namespace Velcoda\Commands;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Velcoda\Commands\Commands\GenerateRequestHandler;
use Velcoda\Commands\Commands\GenerateSerializer;
use Velcoda\Commands\Commands\GenerateTransactionFlow;
use Velcoda\Commands\Commands\GenerateUseCase;
use Velcoda\Commands\Commands\VelcodaAccessKeyCommand;

class CommandsServiceProvider extends ServiceProvider
{
    private $commands = [
        GenerateUseCase::class,
        GenerateRequestHandler::class,
        GenerateTransactionFlow::class,
        GenerateSerializer::class,
        VelcodaAccessKeyCommand::class,
    ];

    /**
     * Boot application service provider
     *
     * @return void
     */
    public function boot()
    {
        $this->commands($this->commands);
    }
}
